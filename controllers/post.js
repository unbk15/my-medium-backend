const formidable = require("formidable");
const Post = require("./../models/post");
const fs = require("fs");
const { errorHandler } = require("./../helpers/dbErrorHandling");

exports.home = (req, res) => {
  res.status(400).json({ msg: "This is the home route" });
};

exports.create = (req, res) => {
  let form = new formidable.IncomingForm();
  form.keepExtensions = true;
  form.parse(req, (err, fields, files) => {
    if (err) {
      return res.status(400).json({ error: "Something went wrong" });
    }
    const { title, description, category, author } = fields;
    if (!title || !description || !category || !author) {
      return res.status(400).json({ error: "All fields are requried" });
    }

    // create new Post
    let post = new Post(fields);
    if (files.photo) {
      if (files.photo.size > 1000000) {
        return res.stat(400).json({ error: "Image should be less than 1mb" });
      }
      post.photo.data = fs.readFileSync(files.photo.path);
      post.photo.contentType = files.photo.type;
    }

    post.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler(err)
        });
      }
      res.status(200).json(result);
    });
  });
};

exports.allPosts = (req, res) => {
  let order = req.query.order ? req.query.order : "asc";
  let sortBye = req.query.sortBye ? req.query.sortBye : "updatedAt";
  let limit = req.query.limit ? parseInt(req.query.limit) : 0;
  Post.find()
    .populate({ path: "category" })
    .sort([[sortBye, order]])
    .limit(limit)
    .exec((err, posts) => {
      if (err) {
        return res.status(400).json({ msg: errorHandler(err) });
      }
      res.status(200).json(posts);
    });
};

exports.allPostsOfACategory = (req, res) => {
  let category = req.query.category;
  let order = req.query.order ? req.query.order : "asc";
  let sortBye = req.query.sortBye ? req.query.sortBye : "updatedAt";
  let limit = req.query.limit ? parseInt(req.query.limit) : 0;
  Post.find({ category })
    .populate({ path: "category" })
    .sort([[sortBye, order]])
    .limit(limit)
    .exec((err, posts) => {
      if (err) {
        res.status(400).json({ msg: errorHandler(err) });
      }
      res.status(200).json(posts);
    });
};

exports.post = (req, res) => {
  const id = req.params.id;
  Post.findById(id, (err, post) => {
    if (err) {
      return res.status(400).json({ msg: errorHandler(err) });
    }
    res.status(200).json(post);
  });
};
