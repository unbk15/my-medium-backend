const Category = require("./../models/category");

exports.create = (req, res) => {
  const category = new Category(req.body);
  let isCategory;
  Category.find(req.body, (err, result) => {
    if (err) {
      return res.status(400).json({ msg: "Something went wrong" });
    }
  });
  category.save((err, result) => {
    if (err) {
      console.log(err);
      return res.status(400).json({ error: "Category already exits" });
    }
    res.status(200).json({ result });
  });
};

exports.allCategories = (req, res) => {
  Category.find({}, (err, categories) => {
    if (err) {
      res.status(400).json({ error: err });
    }
    res.status(200).json(categories);
  });
};
