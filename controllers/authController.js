const jwt = require("jsonwebtoken");
const { User } = require("./../models/user");
const AppError = require("./../utils/appError");
require("dotenv").config();

const signinToken = id => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN
  });
};

exports.signup = async (req, res, next) => {
  try {
    const newUser = await User.create({
      email: req.body.email,
      password: req.body.password,
      passwordConfirm: req.body.passwordConfirm
    });
    const token = signinToken(newUser._id);
    res.status(201).json({ status: "success", token, data: { user: newUser } });
  } catch (err) {
    res.status(400).json(err);
  }
};

exports.signin = async (req, res, next) => {
  const { email, password } = req.body;
  // 1) Check if email and passwords exits
  if (!email || !password) {
    return next(new AppError("Please provide email and password", 400));
  }
  // 2) Check if the user exits && password is correct
  const user = await User.findOne({ email }).select("+password");
  // 3) If everything is ok, send token to client
  console.log(await user.correctPassword(password, user.password));
  if (!user || !(await user.correctPassword(password, user.password))) {
    return next(new AppError("Incorrect email or password", 401));
  }
  const token = signinToken(user._id);
  res.status(200).json({
    status: "success",
    token
  });
};
