const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "unbk15@gmail.com",
    pass: "Angelegenheit26"
  }
});

exports.email = (req, res) => {
  const { name, sender, message } = req.query;
  const mailOptions = {
    from: `${sender}`,
    to: "unbk15@gmail.com",
    subject: `Email From ${name}`,
    text: `${message} \n ${sender}`
  };

  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      return res.status(400).json({ msg: error });
    } else {
      let response = info.response;
      res.status(200).json({ response });
    }
  });
};
