const express = require("express");
const router = express.Router({ strict: true });
const { email } = require("./../controllers/email");

router.get("/email", email);

module.exports = router;
