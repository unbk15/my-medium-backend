const express = require("express");
const router = express.Router({ strict: true });

const {
  home,
  create,
  post,
  allPosts,
  allPostsOfACategory
} = require("../controllers/post");
router.get("/", home);
router.post("/post/create", create);
router.get("/post/all-posts", allPosts);
router.get("/post/all-posts-by-category", allPostsOfACategory);
router.get("/post/:id", post);

module.exports = router;
