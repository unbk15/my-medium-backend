const express = require("express");
const router = express.Router();
const { create, allCategories } = require("./../controllers/category");

router.post("/category/create", create);
router.get("/category/all-categories", allCategories);

module.exports = router;
