const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");
require("dotenv").config();

//Errors
const AppError = require("./utils/appError");
const globalErrorHandler = require("./controllers/errorControler");

// Routes
const postRoutes = require("./routes/post");
const categoryRoutes = require("./routes/category");
const emailRoutes = require("./routes/email");
const userRoutes = require("./routes/user");

//Middelware
require("./controllers/authController");
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(cors());

// Routes
app.use("/api/v1/", postRoutes);
app.use("/api/v1/", categoryRoutes);
app.use("/api/v1/", emailRoutes);
app.use("/api/v1/", userRoutes);
app.all("*", (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server`), 404);
});

app.use(globalErrorHandler);

// Connect to the db
mongoose
  .connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  .then(() => console.log("database-connected"))
  .catch(err => {
    console.log(err);
  });

// Server
const port = process.env.PORT || 8000;
app.listen(port, () => {
  `Listen To Port ${port}`;
});
