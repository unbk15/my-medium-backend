const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcrypt");

require("dotenv").config();

//simple schema
const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, "Please tell us your email"],
    minlength: 5,
    maxlength: 255,
    unique: true,
    validate: [validator.isEmail, "Please provide a valid email"]
  },
  password: {
    type: String,
    required: [true, "Please provide us your password"],
    minlength: 8,
    maxlength: 255,
    lowercase: true,
    select: false
  },
  passwordConfirm: {
    type: String,
    required: [true, "Please confirm your password"],
    validate: {
      // This only works on SAVE!!!
      validator: function(el) {
        return el === this.password;
      },
      message: "Passwords are not the same"
    }
  }
});

UserSchema.pre("save", async function(next) {
  // 1.Only run this function if password was modified
  if (!this.isModified("password")) return next();
  // 2.hash the password with the cost of 12
  this.password = await bcrypt.hash(this.password, 12);
  // 3.delete the password confirm field
  this.passwordConfirm = undefined;
  next();
});

UserSchema.methods.correctPassword = async function(
  candidatePassword,
  userPassword
) {
  return await bcrypt.compare(candidatePassword, userPassword);
};

exports.User = mongoose.model("User", UserSchema);
