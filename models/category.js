const mongoose = require("mongoose");
const categorySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      required: true,
      maxlength: 32,
      unique: true
    },
    color: {
      type: String,
      trim: true,
      required: true,
      maxlength: 20,
      unique: true,
      lowercase: true
    }
  },
  { timestapms: true }
);

module.exports = mongoose.model("Category", categorySchema);
