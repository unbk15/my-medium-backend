const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;
const postSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      maxlength: 100,
      trim: true,
      required: true
    },
    description: {
      type: String,
      maxlength: 10000,
      required: true
    },
    category: {
      type: ObjectId,
      ref: "Category",
      required: true
    },
    author: {
      type: String,
      maxlength: 20,
      required: true,
      trim: true
    },
    photo: {
      data: Buffer,
      contentType: String
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Post", postSchema);
